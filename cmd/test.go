//nolint:gochecknoglobals,gochecknoinits
package cmd

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/salamander/pkg/test"
)

func init() {
	rootCmd.AddCommand(runTest)

	runTest.Flags().StringVarP(&TestDir, "test-dir", "t", "", "Testfile directory")

	if err := runTest.MarkFlagRequired("test-dir"); err != nil {
		log.Fatal().Msg(err.Error())
	}
}

var runTest = &cobra.Command{
	Use:   "test",
	Short: "Testrunner",
	Run: func(cmd *cobra.Command, args []string) {
		test.Execute(TestDir)
	},
}
