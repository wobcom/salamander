//nolint:gochecknoglobals,gochecknoinits
package cmd

import (
	"io/ioutil"
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/salamander/pkg/live"
	"gitlab.com/wobcom/salamander/pkg/mqtt"
)

func init() {
	rootCmd.AddCommand(liveCmd)

	liveCmd.Flags().StringVarP(&MQTTServer, "mqtt-server", "s", "", "MQTT Server")

	if err := liveCmd.MarkFlagRequired("mqtt-server"); err != nil {
		log.Fatal().Msg(err.Error())
	}

	liveCmd.Flags().StringSliceVarP(&MQTTChannel, "mqtt-channel", "c", []string{}, "MQTT Channel")

	if err := liveCmd.MarkFlagRequired("mqtt-channel"); err != nil {
		log.Fatal().Msg(err.Error())
	}

	liveCmd.Flags().StringVarP(&Username, "username", "u", "", "Username")

	if err := liveCmd.MarkFlagRequired("username"); err != nil {
		log.Fatal().Msg(err.Error())
	}

	liveCmd.Flags().StringVarP(&Password, "password", "p", "", "Password")

	if err := liveCmd.MarkFlagRequired("password"); err != nil {
		log.Fatal().Msg(err.Error())
	}

	liveCmd.Flags().StringVarP(&Decoder, "decoder", "d", "", "Path to decoder")
	liveCmd.Flags().StringVarP(&LogDir, "log-dir", "l", "", "Log Dir")
}

var liveCmd = &cobra.Command{
	Use:   "live",
	Short: "Subscribe to a MQTT Channel",
	Run: func(cmd *cobra.Command, args []string) {
		if LogDir != "" {
			if _, err := os.Stat(LogDir); os.IsNotExist(err) {
				log.Fatal().Msg(err.Error())
			}
		}

		m := &mqtt.Connection{
			Server:   MQTTServer,
			Channel:  MQTTChannel,
			Username: Username,
			Password: Password,
			LogDir:   LogDir,
		}

		if Decoder != "" {
			content, err := ioutil.ReadFile(Decoder)
			if err != nil {
				log.Fatal().Msg(err.Error())
			}
			m.Decoder = string(content)
		}

		live.Execute(m)
	},
}
