// nolint: gochecknoglobals, gochecknoinits
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("salamander %s, commit %s, %s", version, commit, date)
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
