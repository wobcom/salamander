package validate

import (
	"fmt"
	"strings"

	"github.com/xeipuuv/gojsonschema"
)

func Valid(schemaURL string, data []byte) error {
	schemaLoader := gojsonschema.NewReferenceLoader(schemaURL)
	documentLoader := gojsonschema.NewBytesLoader(data)

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return fmt.Errorf("%w", err)
	}

	if !result.Valid() {
		es := []string{}

		for _, desc := range result.Errors() {
			es = append(es, desc.String())
		}

		return fmt.Errorf("not valid: %s", strings.Join(es, " / ")) // nolint: goerr113
	}

	return nil
}
