package mqtt

import (
	"fmt"

	"github.com/buger/jsonparser"
	"github.com/davecgh/go-spew/spew"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/salamander/pkg/decode"
	"gitlab.com/wobcom/salamander/pkg/utils"
)

// nolint:funlen,cyclop
func (m *Connection) MsgHandler(client mqtt.Client, msg mqtt.Message) {
	// Ringing the bell.
	fmt.Print("\a")

	applicationName, err := jsonparser.GetString(msg.Payload(), "applicationName")
	if err != nil {
		log.Error().Caller().Msg(err.Error())
	}

	devEUI, err := jsonparser.GetString(msg.Payload(), "devEUI")
	if err != nil {
		log.Error().Caller().Msg(err.Error())
	}

	deviceName, err := jsonparser.GetString(msg.Payload(), "deviceName")
	if err != nil {
		log.Error().Caller().Msg(err.Error())
	}

	data, err := jsonparser.GetString(msg.Payload(), "data")
	if err != nil {
		log.Error().Caller().Msg(err.Error())
	}

	logger := log.With().
		Str("applicationName", applicationName).
		Str("deviceName", deviceName).
		Str("devEUI", devEUI).
		Logger()

	logger.Info().Msg("got message")
	utils.PrettyJSONPrint(msg.Payload())

	// prints hex
	bh, err := utils.Base64Decode(data)

	if err != nil {
		logger.Error().Caller().Msg(err.Error())
	} else {
		logger.Info().Msg("decoded Base64 payload to HEX")
		fmt.Print(spew.Sdump(bh))
	}

	bhs, err := utils.Base64ToHexString(data)
	if err != nil {
		logger.Error().Caller().Msg(err.Error())
	}

	// Save incoming json files.
	if m.LogDir != "" {
		m.WG.Add(1)

		go func() {
			err := utils.SavePrettyJSON(msg.Payload(), bhs, m.LogDir, &m.WG)
			if err != nil {
				log.Error().Caller().Msg(err.Error())
			}
		}()
	}

	// Decode the data.
	if m.Decoder != "" {
		logger.Info().Msg("trying to decode")

		err := decode.Live(m.Decoder, msg.Payload())
		if err != nil {
			logger.Error().Caller().Msg(err.Error())
		}
	}
}
