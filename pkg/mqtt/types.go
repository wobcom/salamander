package mqtt

import "sync"

type Connection struct {
	Channel  []string
	Decoder  string
	LogDir   string
	Password string
	Server   string
	Username string
	WG       sync.WaitGroup
}
