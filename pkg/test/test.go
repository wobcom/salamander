package test

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/wobcom/salamander/pkg/decode"
	"gitlab.com/wobcom/salamander/pkg/validate"
)

type tT struct{}

// Errorf runs when something with assert goes wrong and exists fatally.
func (t tT) Errorf(format string, args ...interface{}) {
	log.Fatal().Msgf(format, args...)
}

type Data struct {
	Expected json.RawMessage `json:"_expected"`
	Payload  json.RawMessage `json:"_payload"`
	Data     json.RawMessage `json:"_data"`
}

type Case struct {
	dir     string
	decoder string   // Decoder string.
	tests   []string // Testfile paths.
}

func (c *Case) Test() {
	logger := log.With().Str("dir", c.dir).Logger()
	t := tT{}
	assert := assert.New(t)

	for _, tPath := range c.tests {
		testFileLogger := logger.With().Str("testFile", filepath.Base(tPath)).Logger()
		testFileLogger.Info().Msg("running...")

		data, err := ioutil.ReadFile(tPath)
		if err != nil {
			testFileLogger.Fatal().Msg(err.Error())
		}

		expected := &Data{}
		err = json.Unmarshal(data, expected)

		if err != nil {
			testFileLogger.Fatal().Msg(err.Error())
		}

		if len(expected.Expected) == 0 {
			testFileLogger.Fatal().Msg(`could not load "expected"`)
		}

		// Test if decoded payload is equal "expected".
		result, err := decode.JSON(c.decoder, data)
		assert.NoError(err)
		assert.JSONEq(string(expected.Expected), result)

		// Test if "schema" key is present.
		schema := make(map[string]interface{})
		err = json.Unmarshal(expected.Expected, &schema)
		assert.NoError(err)

		s, ok := schema["schema"]

		if !ok {
			// assert.FailNow(`"schema" not defined`)
			logger.Warn().Msg(`"schema" not defined`)
		}

		if ok {
			// Run through validator.
			if err := validate.Valid(s.(string), expected.Expected); err != nil {
				assert.FailNow(err.Error())
			}
		}
	}
}

//nolint:nestif,cyclop
func Execute(d string) {
	cases := []*Case{}

	dirs, err := ioutil.ReadDir(d)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	for _, i := range dirs {
		if i.IsDir() && i.Name() != ".git" && i.Name() != "node_modules" {
			logger := log.With().Str("dir", i.Name()).Logger()
			_, err := os.Stat(path.Join(d, i.Name(), "decoder.js"))

			if os.IsNotExist(err) {
				logger.Info().Msg("no decoder.js. skipping...")

				continue
			}

			decoder, err := ioutil.ReadFile(path.Join(d, i.Name(), "decoder.js"))
			if err != nil {
				logger.Fatal().Msg(err.Error())
			}

			c := Case{
				i.Name(),
				string(decoder),
				[]string{},
			}

			_, err = os.Stat(path.Join(d, i.Name(), "testdata"))
			if os.IsNotExist(err) {
				logger.Info().Msg("no testdata. skipping...")

				continue
			}

			tDs, err := ioutil.ReadDir(path.Join(d, i.Name(), "testdata"))
			if err != nil {
				logger.Fatal().Msg(err.Error())
			}

			for _, td := range tDs {
				if strings.Contains(td.Name(), ".json") {
					c.tests = append(c.tests, path.Join(d, i.Name(), "testdata", td.Name()))
				}
			}

			cases = append(cases, &c)
		}
	}

	for _, c := range cases {
		c.Test()
	}
}
