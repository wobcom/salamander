//nolint:gochecknoglobals,goerr113,gomnd
package decode

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"github.com/buger/jsonparser"
	"github.com/robertkrimen/otto"
	"gitlab.com/wobcom/salamander/pkg/utils"
)

var maxExecutionTime = time.Second / 2

func Decode64(s string) ([]byte, error) {
	b, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return []byte{}, err
	}

	return b, nil
}

func Base64ToHexString(s string) (string, error) {
	p, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(p), nil
}

// BinaryToJSON encodes the given binary payload to JSON.
func BinaryToJSON(fPort uint8, variables map[string]string, decodeScript string, b []byte) ([]byte, error) {
	decodeScript += "\n\nDecode(fPort, bytes, variables);\n"

	vars := make(map[string]interface{})

	vars["fPort"] = fPort
	vars["bytes"] = b
	vars["variables"] = variables

	v, err := executeJS(decodeScript, vars)
	if err != nil {
		return nil, errors.Wrap(err, "execute js error")
	}

	return json.Marshal(v)
}

func executeJS(script string, vars map[string]interface{}) (out interface{}, err error) {
	defer func() {
		if caught := recover(); caught != nil {
			err = fmt.Errorf("%s", caught)
		}
	}()

	vm := otto.New()
	vm.Interrupt = make(chan func(), 1)
	vm.SetStackDepthLimit(32)

	for k, v := range vars {
		if err := vm.Set(k, v); err != nil {
			return nil, err
		}
	}

	go func() {
		time.Sleep(maxExecutionTime)
		vm.Interrupt <- func() {
			panic(errors.New("execution timeout"))
		}
	}()

	var val otto.Value
	val, err = vm.Run(script)

	if err != nil {
		fmt.Println(err)

		return nil, err
	}

	return val.Export()
}

func Live(script string, data []byte) error {
	pl, err := jsonparser.GetString(data, "data")
	if err != nil {
		return err
	}

	bp, err := Decode64(pl)
	if err != nil {
		return err
	}

	fPort, err := jsonparser.GetInt(data, "fPort")
	if err != nil {
		return err
	}

	out, err := BinaryToJSON(
		uint8(fPort),
		map[string]string{},
		script,
		bp,
	)
	if err != nil {
		return err
	}

	utils.PrettyJSONPrint(out)

	return nil
}

// Run runs the script and pretty outputs results.
func Run(script string, data []byte) error {
	pl, err := jsonparser.GetString(data, "_data", "data")
	if err != nil {
		return err
	}

	bp, err := Decode64(pl)
	if err != nil {
		return err
	}

	fPort, err := jsonparser.GetInt(data, "_data", "fPort")
	if err != nil {
		return err
	}

	out, err := BinaryToJSON(
		uint8(fPort),
		map[string]string{},
		script,
		bp,
	)
	if err != nil {
		return err
	}

	utils.PrettyJSONPrint(out)

	return nil
}

// JSON runs script and returns its JSON string.
func JSON(script string, data []byte) (string, error) {
	pl, err := jsonparser.GetString(data, "_data", "data")
	if err != nil {
		return "", err
	}

	bp, err := Decode64(pl)
	if err != nil {
		return "", err
	}

	fPort, err := jsonparser.GetInt(data, "_data", "fPort")
	if err != nil {
		return "", err
	}

	out, err := BinaryToJSON(
		uint8(fPort),
		map[string]string{},
		script,
		bp,
	)
	if err != nil {
		return "", err
	}

	return string(out), nil
}
